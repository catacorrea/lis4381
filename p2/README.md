> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

### Assignment P2 Requirements:

*Course Work Links:*



#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, Screenshots as per below examples, Link to local lis4381 web app: http://localhost/repos/lis4381/
* The following screenshots:
    - Failed validation
    - Before *and* after successful edit
    - Deleted record
    - RSS feed

*Carousel Homepage*:

![Carousel](img/cbg.png)
![Carousel](img/hub.png)
![Carousel](img/link.png)

*Index.php*:

![Index.php](img/indexnew.png)

*Edit Petstore*:

![edit](img/addnew.png)

*Successful validation*:

![passed](img/passedval.png)

*Screenshot of Delete Prompt*:

![error](img/deleteconf.png)

*Screenshot of Failed Validation*:

![error](img/erroredit.png)

*Successfully Deleted Validation*:

![deleted](img/deletedval.png)

*Screenshot of RSS Feed*:

![rss](img/rssfeed.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")
