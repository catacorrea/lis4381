> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

### Assignment 4 Requirements:

*Course Work Links:*

#### README.md file should include the following items:
- Coursetitle, your name, assignment requirements, as per A1
- Link to local lis4381 web app
- Provide Bitbucketread-only access to lis4381 repo
- Include links to the other assignment repos you created inREADME.md
- *Note*:the carousel *must*contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills.

#### Assignment Screenshots:

LINK TO LOCAL WEB APP: [Local Web App Link](http://localhost/repos/index.php "My A4 Web App")

*Screenshot of Portfolio Main Page With All Slides*:

*Slide One*:
![AMPPS Installation Screenshot](img/home.png)

*Slide Two*:
![AMPPS Installation Screenshot](img/hometwo.png)

*Slide Three*:
![AMPPS Installation Screenshot](img/homethree.png)


*Screenshot of A4 Data Validation Failing*:

![AMPPS Installation Screenshot](img/failed.png)

*Screenshot of A4 Data Validation Successful*:

![JDK Installation Screenshot](img/passed.png)


*Screenshot of Skillset 10*:

![JDK Installation Screenshot](img/ten.png)

*Screenshot of Skillset 11*:

![JDK Installation Screenshot](img/eleven.png)

*Screenshot of Skillset 12*:

![JDK Installation Screenshot](img/twelve.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")

