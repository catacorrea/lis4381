<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that showcases the work I completed during my senior year at FSU.">
		<meta name="author" content="Catalina Correa">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Course title, your name, assignment requirements, as per A1, Screenshot of running application’s first user interface, Screenshot of running application’s second user interface, Screenshot of skillsets 1, 2, and 3 </p>

				<h4>Screenshot of First User Interface Running</h4>
				<img src="img/appfront.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Screenshot of Second User Interface Running</h4>
				<img src="img/apprecipe.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Even or Odd Skillset Screenshot</h4>
				<img src="img/evenorodd.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>Largest Number Skillset Screenshot</h4>
				<img src="img/largestnumber.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<h4>Arrays and Loops Skillset Screenshot</h4>
				<img src="img/arraysandloops.png" class="img-responsive center-block" alt="AMPPS Installation">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
