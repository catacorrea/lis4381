> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

### Assignment 2 Requirements:

#### README.mdfile should include the following items:
1. Coursetitle, your name, assignment requirements, as per A1
2. Screenshot of running application’s first user interface
3. Screenshot of running application’s second user interface
4. Screenshot of skillsets 1, 2, and 3

#### Assignment Screenshots:

*Screenshot of first and second user interface*:

| ![First User Interface Screenshot](img/appfront.png) | ![Second User Interface Schreenshot](img/apprecipe.png)  |

*Screenshots of Skillsets 1 - 3*:

|  ![Skillset 1 Screenshot](img/evenorodd.png)  | ![Skillset 2 Screenshot](img/largestnumber.png) |
![Skillset 3 Screenshot](img/arraysandloops.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")
