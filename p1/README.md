> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

### Project 1 Requirements:

#### README.md file should include the following items:

* Create a launcher icon image and display it in both activities (screens)
* Must add background color(s) to both activities
* Must add border around image and button
* Must add text shadow(button)
* Screenshots of first and second running user interface
* Skillset 7,8,9 screenshots

#### Project Screenshots:

*Screenshots of interface running*:

|  ![Front App Screenshot](img/frontapp.png)  |  ![Back App Screenshot](img/backapp.png)  |

*Screenshots of Skillsets 7-9*:

| ![Skillsets Screenshot](img/skillseven.png)  | ![Skillsets Screenshot](img/skilleight.png)  | ![Skillsets Screenshot](img/skillnine.png) |   



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")
