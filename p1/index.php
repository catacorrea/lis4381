<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>CRSXXXX - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Create a launcher icon image and display it in both activities (screens), Must add background color(s) to both activities, Must add border around image and button, Must add text shadow(button), Screenshots of first and second running user interface, and Skillset 7,8,9 screenshots
				</p>

				<h4>Screenshot of First User Interface Running</h4>
				<img src="img/appfront.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Screenshot of Second User Interface Running</h4>
				<img src="img/appback.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Skillset Seven Screenshot</h4>
				<img src="img/ssseven.png" class="img-responsive center-block" alt="AMPPS Installation">

				<h4>Skillset Eight Screenshot</h4>
				<img src="img/sseight.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<h4>Skillset Nine Screenshot</h4>
				<img src="img/ssnine.png" class="img-responsive center-block" alt="AMPPS Installation">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
