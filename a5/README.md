> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

### Assignment 5 Requirements:

-  Server-side validation on petstore table
- Be able to add data to existing form
- Screenshots for skillsets 13-15

#### Link to Local Repo:

[Local Web App Link](http://localhost/repos/index.php "My A5 Web App")

#### Assignment Screenshots:

*Screenshot of Home Table Screen*:

![AMPPS Installation Screenshot](img/table.png)

*Screenshot of Error.php*:

![AMPPS Installation Screenshot](img/error.png)

*Screenshot of skillset 13*:

![AMPPS Installation Screenshot](img/ss13.png)

*Screenshots of skillset 14 - Simple Calculator*:

![JDK Installation Screenshot](img/ss14a.png)
![JDK Installation Screenshot](img/ss14add.png)
![JDK Installation Screenshot](img/ss14d.png)
![JDK Installation Screenshot](img/ss14div.png)

*Screenshot of skillset 15 - Write/Read File*:

![Android Studio Installation Screenshot](img/input.png)
![Android Studio Installation Screenshot](img/output.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")