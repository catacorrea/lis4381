> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Git
    - Create Bitbucket account, make a repo
    - Connect local to Bitbucket repository
    - Install AMPPS
    - Install JKD
    - Install Android Studio
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Coursetitle, your name, assignment requirements, as per A1
    - Screenshot of running application’s first user interface
    - Screenshot of running application’s second user interface
    - Screenshot of skillsets 1, 2, and 3
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of ERD
    - Screenshot of running application's first user interface 
    - Screenshot of running application's second user interface
    - Links to a3.mwb & a3.sql 
    - Screenshots of skillsets 4,5, and 6
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Link to local lis4381 web app: http://localhost/repos/
    - Provide Bitbucketread-only access to lis4381 repo
    - Include links to the other assignment repos you created inREADME.md
    - *Note*:the carousel *must*contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills.    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server-side validation on petstore table
    - Be able to add data to existing form
    - Screenshots for skillsets 13-15
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a launcher icon image and display it in both activities (screens)
    - Must add background color(s) to both activities
    - Must add border around image and button
    - Must add text shadow(button)
    - Screenshots of first and second running user interface
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Coursetitle, your name, assignment requirements, Screenshots as per below examples, Link to local lis4381 web app: http://localhost/repos/lis4381/
    - The following screenshots:
    - Failed validation
    - Before *and* after successful edit
    - Deleted record
    - RSS feed

