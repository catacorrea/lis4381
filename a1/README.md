> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Catalina Correa

### Assignment 1 Requirements:

1. Distributed Version Control
2. Development Installations
3. Chapter Questions

#### README.md file should include the following items:

1. Screenshot of ampps installation running (#1 above);
2. Screenshot of running JDK java Hello (#2 above);
3. Screenshot of running Android Studio My First App (#3 above, example below); (Note: use *your name* instead of “Hello World!”)
4. git commands w/short descriptions (“Lesson 3b - Version Control Systems: Course Configuration”);
5. Bitbucket repo links:
a. This assignment, and
b. The completed tutorial repo above (bitbucketstationlocations). (See link in screenshot below.)

> #### Git commands w/short descriptions:

1. git init : Create an empty Git repository or reinitialize an existing one
2. git status : Shows the working tree status 
3. git add : Add file contents to the index  
4. git commit : Record changes to the repository
5. git push : Update remote refs along with associated objects
6. git pull : Fetch from and integrate with another repository or a local branch
7. git clone /path/to/repository : Create a working copy of a local repository



#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javaassignment.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")

