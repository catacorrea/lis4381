> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web App Development

## Catalina Correa

### Assignment 3 Requirements:

*Course Work Links:*

#### README.md file should include the following items:

* Create a launcher icon
* Screenshot of ERD
* Screenshot of running application's first user interface 
* Screenshot of running application's second user interface
* Links to the following files:
    * a3.mwb
    * a3.sql 
* Screenshots of skillsets 4, 5, and 6

#### Assignment Screenshots and Links:

*Screenshot of ERD:*

![ERD Screenshot](img/erd.png)

[a3 mwb file](docs/a3.mwb "A3 MWB File")

[a3 sql file](docs/a3.sql "A3 SQL File")

*Screenshots of user interface running:*

| ![Screenshot of First User Interface Running](img/userintone.png)  | ![Screenshot of Second User Interface Running](img/userinttwo.png)  |  ![Screenshot of Second User Interface Running](img/userintthree.png)  |

*Screenshots of Skillsets:*

| ![Screenshot of Skillset 4 DecisionStructures](img/decisionstructure.png)  | ![Screenshot of Skillset 5 Methods](img/methods.png)   | ![Screenshot of Skillset 6 Methods](img/methodslast.png)  |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/catacorrea/bitbucketstationlocations/ "Bitbucket Station Locations")

