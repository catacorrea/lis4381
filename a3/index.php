<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
				<strong>Description:</strong> Assignment Requirements include: Create a launcher icon
Screenshot of ERD, Screenshot of running application's first user interface, Screenshot of running application's second user interface, Links to the following files: a3.mwb & a3.sql, and Screenshots of skillsets 4, 5, and 6
				</p>

				<h4>Screenshot of ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="ERD">

				<h4>First User Interface Running</h4>
				<img src="img/userintone.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Second User Interface Running</h4>
				<img src="img/userinttwo.png" class="img-responsive center-block" alt="Android Studio Installation">
				
				<h4>Third User Interface Running</h4>
				<img src="img/userintthree.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>Decision Structures Skillset</h4>
				<img src="img/decisionstructure.png" class="img-responsive center-block" alt="Even Or Odd Skillset">

				<h4>Methods Skillset Screenshot</h4>
				<img src="img/methods.png" class="img-responsive center-block" alt="Largest Number Skillset">
				
				<h4>Other Methods Skillset Screenshot</h4> 
				<img src="img/methodslast.png" class="img-responsive center-block" alt="Arrays and Loops Skillset">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
